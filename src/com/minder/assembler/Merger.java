package com.minder.assembler;

import java.util.ArrayList;
import java.util.List;

public class Merger {
	private static class Header {
		public final String name;
		public List<String> data;
		
		public Header(String name) {
			this.name = name;
			data = new ArrayList<String>();
		}
		
		@Override
		public boolean equals(Object other) {
			if (other instanceof Header) {
				Header otherHeader = (Header)other;			
				return this.name.compareTo(otherHeader.name) == 0;
			} else {
				return false;
			}
		}
	}
	
	private List<Header> file;
	
	public Merger(List<String> originalFile) {
		file = createHeaderList(originalFile);
	}
	
	/**
	 * Creates a string containg the current data
	 * @return A string with all file data
	 */
	public String getFile() {
		StringBuilder sb = new StringBuilder();
		
		for (Header h : file) {
			sb.append(h.name).append(System.getProperty("line.separator"));
			
			for (String s : h.data) {
				sb.append(s).append(System.getProperty("line.separator"));
			}
			sb.append(System.getProperty("line.separator"));
		}
		
		sb.delete(sb.length() - (1 + System.getProperty("line.separator").length()), sb.length() - 1);
		sb.append("End_of_dump_file");
		
		return sb.toString();
	}
	
	/**
	 * Will merge the new file onto the old
	 * overwriting everything in the old one
	 * if it exists in the new one
	 * @param newFile The file to merge
	 */
	public void merge(List<String> newFile) {
		List<Header> newHeaders = createHeaderList(newFile);
		
		for (int i = 0; i < file.size(); i++) {
			for (int j = 0; j < newHeaders.size(); j++) {
				// check if same list of data
				if (file.get(i).equals(newHeaders.get(j))) {
					// replace old contents with new ones
					System.out.println("replaced header " + newHeaders.get(j).name);
					file.remove(i);
					file.add(i, newHeaders.get(j));
					break;
				}
			}
		}
	}
	
	private static List<Header> createHeaderList(List<String> data) {
		List<Header> result = new ArrayList<Header>();
		
		Header currentHeader = null;
		for (String s : data) {
			if (s.isEmpty()) {
				result.add(currentHeader);
				currentHeader = null;
				continue;
			}
			
			if (currentHeader == null) {
				// next line should be a header
				if (s.split(" ").length != 1)
					throw new RuntimeException("Badly formatted file - expected header. Got something else\n" + s);
				
				currentHeader = new Header(s);
			} else {
				if (s.compareTo("End_of_dump_file") == 0) {
					result.add(currentHeader);
					break;
				}

				currentHeader.data.add(s);
			}
		}
		
		return result;
	}
}
