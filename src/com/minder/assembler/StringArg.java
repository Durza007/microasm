package com.minder.assembler;

/**
 * A utility class for handling and parsing the text that is being received from the server
 */
public class StringArg {
	public final static char COMMENT_CHAR = ';';
	public final static char LABEL_CHAR = ':';
	
	public final String string;
	private final boolean hasColon;
	private StringArg next;

	private StringArg(String string, boolean hasColon) {
		this.string = string;
		this.hasColon = hasColon;
	}

    /**
     * Get the length of the linked list
     * @return The length of the linked list
     */
	public int length() {
		if (next != null) {
			return 1 + next.length();
		}
		
		return 1;
	}

    /**
     * Get the rest of the linked list
     * @return The StringArg following this one
     */
	public StringArg rest() {
		return next;
	}

    /**
     * Appends a StringArg to the end of the linked list
     * @param arg The StringArg to add
     */
	public void append(StringArg arg) {
		if (next == null) {
			next = arg;
		} else {
			next.append(arg);
		}
	}

    /**
     * Gets the StringArg at a specific index
     * @param index The index to get a StringArg at
     * @return A StringArg
     * @throws ArrayIndexOutOfBoundsException if the index was out of bounds
     */
	public StringArg get(int index) {
        if (index < 0)
            throw new ArrayIndexOutOfBoundsException(index);

		if (index == 0) {
			return this;
		} else {
			if (next != null) {
				return next.get(index - 1);
			} else {
				throw new ArrayIndexOutOfBoundsException("Reached end of linked list");
			}
		}
	}
	
	/**
	 * Simply checks if this string starts with a colon
	 */
	public boolean isLabel() {
		return hasColon;
	}

	@Override
	public String toString() {
		if (next == null) {
			return string;
		}
		return string + " " + next.toString();
	}

	/**
	 * Parses a string of parameters into StringArgs
	 * @param args The string to parse
	 * @return A StringArg containing the parameters
	 */
	public static StringArg parse(String args) {
		//parse tabs
		args = args.replaceAll("\\s+", " ").trim();
		
		StringArg result;
		int firstWhitespace = args.indexOf(' ');
		
		if (args.isEmpty())
			return null;
		
		if (firstWhitespace == -1) {
			if (args.charAt(0) == COMMENT_CHAR)
				return null;
			
			return new StringArg(args, args.charAt(0) == LABEL_CHAR);
		} else {
			if (args.charAt(0) == LABEL_CHAR) {
				result = new StringArg(args.substring(0, firstWhitespace), true);
			} else if (args.charAt(0) == COMMENT_CHAR) {
				return null;
			} else {	
				result = new StringArg(args.substring(0, firstWhitespace), false);
			}
		}
		
		while (true) {
			int oldWhitespace = firstWhitespace;
			firstWhitespace = args.indexOf(' ', firstWhitespace + 1);

			if (oldWhitespace + 1 >= args.length())
				return result;
			
			if (args.charAt(oldWhitespace + 1) == COMMENT_CHAR)
				return result;
			
			if (firstWhitespace == -1) {
				result.append(new StringArg(args.substring(oldWhitespace + 1), args.charAt(oldWhitespace + 1) == LABEL_CHAR));
				return result;
			} else {
				result.append(new StringArg(args.substring(oldWhitespace + 1, firstWhitespace), args.charAt(oldWhitespace + 1) == LABEL_CHAR));
			}
		}
	}
}