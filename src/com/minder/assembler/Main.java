package com.minder.assembler;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

public class Main {
	private static void printArgError() {
		System.out.println("ARGS: -asm <file-to-assemble> (<output-name>)");
		System.out.println("      -micro (<file-to-load>)");
		System.out.println("      -merge <original-file> <files-to-merge> ...");
		System.exit(0);
	}
	
	private static List<String> loadFileAsStringList(String file) throws IOException {
		List<String> returnList = new LinkedList<String>();
		
	    BufferedReader br = new BufferedReader(new FileReader(file));
	    try {
	        String line = br.readLine();

	        while (line != null) {
	        	returnList.add(line);
	            line = br.readLine();
	        }
	    } finally {
	        br.close();
	    }
	    
	    return returnList;
	}
	
	public static void main(String[] args) throws IOException {	
		if (args.length < 1) {
			printArgError();
			return;
		}
		
		if (args[0].compareTo("-asm") == 0) {
			// read assembler code
			final List<String> assemblerLines = loadFileAsStringList(args[1]);
		    
			Assembler asm = null;
			try {
				asm = new Assembler(assemblerLines);
			} catch (RuntimeException e) {
				System.out.println(e.getMessage());
			}
			
			if (asm == null)
				return;
		    
		    String out = "out.mia";
		    if (args.length == 3)
		    	out = args[2];
		    
			final OutputStream output = new FileOutputStream(out);
			final PrintStream printOut = new PrintStream(output);
		    HexWriter writer = new HexWriter(printOut);
		    writer.setValueLength(4);
		    
		    printOut.println("PM:");
		    writer.write(asm.getInstructionData(), 256);
		    printOut.println("End_of_dump_file");
		    printOut.close();
		} else if (args[0].compareTo("-micro") == 0) {
			if (args.length == 2) {
				new MikroEditor(args[1]);
			} else {
				new MikroEditor();
			}
		} else if (args[0].compareTo("-merge") == 0) {
			final int length = args.length - 1;
			if (length == 1)
				return;
			
			List<String> lines = loadFileAsStringList(args[1]);
			Merger merger = new Merger(lines);
			
			for (int i = 1; i < length; i++) {
				lines = loadFileAsStringList(args[i + 1]);
				merger.merge(lines);
			}
			
			final OutputStream output = new FileOutputStream(args[1]);
			final PrintStream printOut = new PrintStream(output);
			printOut.print(merger.getFile());
			printOut.close();
		} else {
			printArgError();
		}
	}

}
