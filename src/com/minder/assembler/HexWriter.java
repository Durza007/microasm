package com.minder.assembler;

import java.io.PrintStream;
import java.util.List;

public class HexWriter {
	private final PrintStream out;
	private String zeroes;

	public int getValueLength() {
		return zeroes.length();
	}

	public void setValueLength(int valueLength) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < valueLength; i++) {
			sb.append("0");
		}
		zeroes = sb.toString();
	}

	public HexWriter(PrintStream output) {
		this.out = output;
	}
	
	public void write(List<Integer> valueList, int totalSize) {
		int rowNumber = 0;
		final String rowZeroes = "00";
		
		for (Integer i : valueList) {
			final String hex = getFormatedHexString(Integer.toHexString(i), zeroes);
			final String rowNr = getFormatedHexString(Integer.toHexString(rowNumber), rowZeroes);
			
			out.println(rowNr + ": " + hex);
			rowNumber++;
		}
		
		if (rowNumber < totalSize) {
			for (int i = rowNumber; i < totalSize; i++) {
				final String hex = getFormatedHexString(Integer.toHexString(0), zeroes);
				final String rowNr = getFormatedHexString(Integer.toHexString(i), rowZeroes);
				
				out.println(rowNr + ": " + hex);
			}
		} else if (rowNumber > totalSize) {
			throw new RuntimeException("Number of rows needed greather than totalSize");
		}
	}
	
	public static String getFormatedHexString(String hex, String zeroes ) {
		final int length = hex.length();
		final int wantedLength = zeroes.length();
		
		if (length <= wantedLength) {
			hex = zeroes.substring(0, wantedLength - length).concat(hex);
		} else {
			throw new RuntimeException("Value too big! " + hex);
		}
		
		return hex;
	}

}
