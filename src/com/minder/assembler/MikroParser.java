package com.minder.assembler;

public class MikroParser {
	private final static String[] ALU = {
		"NOP",
		"Buss",
		"Buss' (ettkomplement)",
		"0",
		"AR + Buss",
		"AR - Buss",
		"AR & Buss",
		"AR | Buss",
		"AR + Buss (inga flaggor)",
		"AR << 1",
		"ARHR << 1",
		"AR >> 1 (aritmetiskt)",
		"ARHR >> 1 (aritmetiskt)",
		"AR >> 1 (logiskt)",
		"Rotera AR �t v�nster",
		"Rotera ARHR �t v�nster"
	};
	
	private final static String[] SEQ = {
		"uPC++",
		"uPC = K1",
		"uPC = K2",
		"uPC = 0",
		"SEQ: EJ DEF",
		"uPC = uADR",
		"uJSR uADR",
		"uRTS",
		"Hopp till uADR om Z = 1",
		"Hopp till uADR om N = 1",
		"Hopp till uADR om C = 1",
		"Hopp till uADR om O = 1",
		"Hopp till uADR om L = 1",
		"SEQ: EJ DEF",
		"SEQ: EJ DEF",
		"HALT"
	};
	
	private final static String[] TB = {
		"NOP",
		"IR",
		"PM",
		"PC",
		"AR",
		"HR",
		"GRx",
		"Styrord"
	};
	
	private final static String[] FB = {
		"NOP",
		"IR",
		"PM",
		"PC",
		"-",
		"HR",
		"GRx",
		"ASR"
	};
	
	private final static String[] LC = {
		"NOP",
		"LC--",
		"LC = Buss",
		"LC = uADR"
	};
	
	public static String getComments(String code) {
		StringBuilder sb = new StringBuilder();
		code = code.replaceAll("\\s+", " ");
		String[] lines = code.split(" ");
		
		for (int i = 0; i < lines.length; i++) {
			if (lines[i].length() == 25) {
				try {
					boolean flagError = false;
					int uCode = Integer.parseInt(lines[i], 2);
					
					int alu = (uCode & 0x1E00000) >> 21;
					int tb = (uCode & 0x01C0000) >> 18;
					int fb = (uCode & 0x0038000) >> 15;
					boolean s = ((uCode & 0x0004000) >> 14) == 1;
					boolean p = ((uCode & 0x0002000) >> 13) == 1;
					int lc = (uCode & 0x0001800) >> 11;
					int seq = (uCode & 0x0000780) >> 7;
					int uAdr = (uCode & 0x000007F);
					
					if (alu != 0) {
						String t = ALU[alu];
						// kolla om vi l�ser fr�n buss
						if (alu >= 1 && alu <= 8 && alu != 3) {
							t = t.replace("Buss", TB[tb]);
							
							if (tb == 0) flagError = true;
						}
						
						sb.append("AR := ").append(t).append(", ");
					}
					
					// om vi l�ser fr�n styrordet slutar alla andra bitar att fungera
					if (tb != 7) {
						if (fb != 0) {
							if (fb == 4 || tb == 0) flagError = true;
							
							sb.append(FB[fb]).append(" := ").append(TB[tb]).append(", ");
						}
						
						if (s) {
							sb.append("GR(M), ");
						} else {
							sb.append("GR(GRx), ");
						}
						
						if (p) {
							sb.append("PC++, ");
						}
						
						if (lc != 0) {
							String t = LC[lc];
							if (lc == 2)
								t = t.replace("Buss", TB[tb]);
							
							if (lc == 3)
								t = t.replace("uADR", HexWriter.getFormatedHexString(Integer.toHexString(uAdr), "00"));
							
							sb.append(LC[lc]).append(", ");
						}
						
						String t = SEQ[seq];
						if (seq >= 5 && seq <= 12 & seq != 7) {
							t = t.replace("uADR", HexWriter.getFormatedHexString(Integer.toHexString(uAdr), "00"));
						}
						sb.append(t);
					} else {
						sb.append(SEQ[0]);
					}
					
					if (flagError)
						sb.append(" (!)");
					
					sb.append(System.getProperty("line.separator"));		
				} catch (NumberFormatException e) {
					sb.append("BAD FORMAT!").append(System.getProperty("line.separator"));
				}
			} else {
				sb.append(System.getProperty("line.separator"));
			}
		}
		
		
		return sb.toString();
	}
}
