package com.minder.assembler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Assembler {
	private static class Label {
		public final String label;
		public int address;
		
		public Label(String name) {
			this.label = name;
			this.address = -1;
		}
		
		public Label(String name, int address) {
			this.label = name;
			this.address = address;
		}
		
		public int getRelativeAddress(int row) {
			if (address > row)
				// +1 because PC will be 1 line ahead
				return address - (row + 1); 
			else
				// to jump backwards we need to wrap around
				return 0xFF - (row - address);
		}
	}
	
	private static class LabelToUpdate {
		public final Label label;
		public final int row;
		public final boolean relativeAddress;
		
		public LabelToUpdate(Label label, int row, boolean relativeAddress) {
			this.label = label;
			this.row = row;
			this.relativeAddress = relativeAddress;
		}
	}
	
	private final static String[] opTable = {
		"LOAD",
		"STORE",
		"ADD",
		"SUB",
		"AND",
		"LSR",
		"BRA",
		"SWAP",
		"HALT",
		"CMP",
		"BGE",
		"BEQ",
		"PUSH",
		"POP",
		"JSR",
		"RTS"
	};
	
	private List<Label> labelList;
	private List<LabelToUpdate> labelsToUpdate;
	private List<Integer> instructionData;
	
	public List<Integer> getInstructionData() {
		return this.instructionData;
	}
	
	public Assembler(List<String> code) {
		labelList = new LinkedList<Label>();
		labelsToUpdate = new LinkedList<LabelToUpdate>();
		instructionData = new ArrayList<Integer>();
		
		StringArg codeLine;
		final int length = code.size();
		for (int i = 0; i < length; i++) {
			codeLine = StringArg.parse(code.remove(0));
			
			if (codeLine == null)
				continue;
			
			// check label
			if (codeLine.isLabel()) {
				if (codeLine.rest() == null)
					throw new RuntimeException("On line:" + (i + 1) + " Label can not be alone on row");
				
				setLabel(codeLine.string, instructionData.size());
				codeLine = codeLine.rest();
			}
			
			addInstructionCode(codeLine.string, codeLine.rest() == null ? null : codeLine.rest().string);
		}

		for (LabelToUpdate l : labelsToUpdate) {
			Integer instr = instructionData.get(l.row);
			if (l.label.address == -1)
				throw new RuntimeException("Label: " + l.label.label.substring(1) + " does not exist");
			
			if (l.relativeAddress)
				instr |= l.label.getRelativeAddress(l.row);
			else
				instr |= l.label.address;
			
			instructionData.remove(l.row);
			instructionData.add(l.row, instr);
		}
	}
	
	private int currentLine;
	private void addInstructionCode(String op, String param) {
		currentLine = instructionData.size();
		
		int opCode = -1;
		int argument = 0;
		int address = 0;
		boolean extraArgument = false;
		int extraValue = 0;
		
		op = op.toUpperCase();
		for (int i = 0; i < opTable.length; i++) {
			if (opTable[i].compareTo(op) == 0) {
				opCode = i;
				break;
			}
		}
		
		if (opCode == -1) {
			// assume constant
			address = getAddress(op, false);
			
			// add our instruction
			instructionData.add(address);
			return;
		}
		
		if (param != null) {
			String[] arguments = param.split(",");
			if (arguments.length == 3 || arguments.length == 1) {
				int a = 0;
				int aMode = 0;
				if (arguments.length == 3) {
					int register = getRegister(arguments[0]);
					aMode = getAMode(arguments[1]);
					argument = (register << 2) | aMode;
					
					a = 2;
				}
				
				// get relative address if # of arguments = 0 and opCode is not JSR
				address = getAddress(arguments[a], a == 0 & opCode != 14);
				
				// immediate
				if (aMode == 1) {
					extraValue = address;
					address = 0;
					extraArgument = true;
				}
				
			} else {
				throw new RuntimeException("Arguments are not valid: " + param);
			}
		}
		
		// add our instruction
		instructionData.add((opCode << 12) | (argument << 8) | address);
		
		if (extraArgument) {
			currentLine++;
			instructionData.add(extraValue);
		}
		
		return;
	}
	
	private int getRegister(String reg) {
		reg = reg.toUpperCase();
		if (reg.length() != 3)
			throw new RuntimeException("Bad register: " + reg);
		
		int register = Integer.parseInt(reg.substring(2));
		if (register < 0 || register > 3)
			throw new RuntimeException("Bad register value: " + reg);
		
		return register;
	}
	
	private int getAMode(String a) {
		if (a.length() != 2)
			throw new RuntimeException("Bad A-Mode: " + a);
		
		if (a.charAt(0) == '1') {
			if (a.charAt(1) == '1') {
				return 3;
			} else if (a.charAt(1) == '0') {
				return 2;
			} else {
				throw new RuntimeException("Bad A-Mode: " + a);
			}
		} else if (a.charAt(0) == '0') {
			if (a.charAt(1) == '1') {
				return 1;
			} else if (a.charAt(1) == '0') {
				return 0;
			} else {
				throw new RuntimeException("Bad A-Mode: " + a);
			}
		}
		
		return -1;
	}
	
	private int getAddress(String address, boolean relativeAddress) {
		if (address.charAt(0) == '$') {
			// hex
			return Integer.parseInt(address.substring(1), 16); 
		} else if (address.charAt(0) == ':') { 
			// label
			return getLabel(address, relativeAddress);		
		} else {
			// constant
			return Integer.parseInt(address);
		}
	}
	
	private int getLabel(String label, boolean relativeAddress) {
		label = label.toUpperCase();
		for (Label l : labelList) {
			if (l.label.compareTo(label) == 0) {
				if (l.address != -1) {
					if (relativeAddress)
						return l.getRelativeAddress(currentLine);
					else
						return l.address;
				} else {
					labelsToUpdate.add(new LabelToUpdate(l, currentLine, relativeAddress));
					return 0;
				}
			}
		}
		
		// did not find label - create it
		final Label l = new Label(label);
		labelList.add(l);
		labelsToUpdate.add(new LabelToUpdate(l, currentLine, relativeAddress));
		
		return 0;
	}
	
	private void setLabel(String label, int address) {
		label = label.toUpperCase();
		for (Label l : labelList) {
			if (l.label.compareTo(label) == 0) {
				l.address = address;
				return;
			}
		}
		
		// did not find label - create it
		final Label l = new Label(label, address);
		labelList.add(l);
	}
}
