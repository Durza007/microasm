package com.minder.assembler;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class MikroEditor extends JFrame implements Runnable {
	private static final long serialVersionUID = 1L;

	private JTextField filePathTextField;
	
	private JTextArea lineNumbers;
	private JScrollPane lineNumberScroll;
	
	private JTextArea mikroCode;
	private JScrollPane mikroScroll;
	
	private JTextArea actions;
	private JScrollPane actionsScroll;
	
	private final static Object lock = new Object();
	boolean newCode;
	private String microCode;
	
	public MikroEditor() {
		init();
	}
	
	public MikroEditor(String fileToLoad) throws FileNotFoundException {
		init();
		filePathTextField.setText(fileToLoad);
		
	    BufferedReader br = new BufferedReader(new FileReader(fileToLoad));
	    try {
	    	StringBuffer sb = new StringBuffer();
	        String line = br.readLine();
	        
	        int count = -1;
	        while (line != null) {        	
	        	if (count >= 0) {
		        	int value = Integer.parseInt(line.split(" ")[1], 16);
		        	sb.append(HexWriter.getFormatedHexString(Integer.toBinaryString(value), "0000000000000000000000000"));
		        	sb.append(System.getProperty("line.separator"));
		        	
		        	if (++count > 0x7F) {
		        		sb.deleteCharAt(sb.length() - 1);
		        		break;
		        	}
	        	}
	        	
	        	if (count == -1 && line.contains("MyM:")) {
	        		count = 0;
	        	}

	            line = br.readLine();
	        }
	        
	        mikroCode.setText(sb.toString());
	    } catch (IOException e) {
			e.printStackTrace();
		} finally {
	        try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	    
	    notifyCodeCommentor();
	}
	
	private void init() {
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        
        final JPanel savePanel = new JPanel();
        filePathTextField = new JTextField(40);
        final JButton saveButton = new JButton();
        saveButton.setText("Save");
        saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String[] uLines = mikroCode.getText().replaceAll("\\s+", " ").split(" ");
				List<Integer> codes = new LinkedList<Integer>();
				for (int i = 0; i < uLines.length; i++) {
					try {
						int uCode = Integer.parseInt(uLines[i], 2);
						
						codes.add(uCode);
					} catch (NumberFormatException exc) {
						codes.add(0);
					}
				}
				
				if (codes.size() > 0x80) {
					JOptionPane.showMessageDialog(MikroEditor.this, "Too many rows! Can't save.");
				} else {
					OutputStream output;
					try {
						output = new FileOutputStream(filePathTextField.getText());
						final PrintStream printOut = new PrintStream(output);
						printOut.println("MyM:");
					    HexWriter writer = new HexWriter(printOut);
					    writer.setValueLength(7);
					    writer.write(codes, 0x80);
					    printOut.println("End_of_dump_file");
					    
					    printOut.close();
					    JOptionPane.showMessageDialog(MikroEditor.this, "Save finished!");
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
        
        savePanel.add(filePathTextField);
        savePanel.add(saveButton);
        this.add(savePanel, BorderLayout.NORTH);       
        
        lineNumbers = new JTextArea();
        lineNumbers.setEditable(false);
        lineNumbers.setColumns(2);
        StringBuilder sb = new StringBuilder();
        
        final String zeroes = "00";
        for (int i = 0; i <= 0x7F; i++) {
        	sb.append(HexWriter.getFormatedHexString(Integer.toHexString(i), zeroes));
        	if (i != 0x7F)
        		sb.append(System.getProperty("line.separator"));
        }
        lineNumbers.setText(sb.toString());
        
        lineNumberScroll = new JScrollPane(lineNumbers);
        lineNumberScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        this.add(lineNumberScroll, BorderLayout.WEST);
		
        final JPanel codeArea = new JPanel();
        codeArea.setLayout(new BorderLayout());
        
        // mikro code editor
		mikroCode = new JTextArea();
		mikroCode.setColumns(20);
		mikroCode.getDocument().addDocumentListener(documentListener);
		mikroCode.setLineWrap(false);
		
		mikroScroll = new JScrollPane(mikroCode);
		mikroScroll.setVerticalScrollBarPolicy(
		                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		mikroScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		mikroScroll.getVerticalScrollBar().addAdjustmentListener(scrollListener);
		codeArea.add(mikroScroll, BorderLayout.WEST);
		
		// actions text area
		actions = new JTextArea();
		actions.setEditable(false);
		
		actionsScroll = new JScrollPane(actions);
		actionsScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		codeArea.add(actionsScroll, BorderLayout.CENTER);
		
		this.add(codeArea, BorderLayout.CENTER);
		
        setSize(640, 480);
        setTitle("MikroEditor");

        setVisible(true);
        
        Thread bgThread = new Thread(this);
        bgThread.setDaemon(true);
        bgThread.start();
	}
	
	private AdjustmentListener scrollListener = new AdjustmentListener() {
		@Override
		public void adjustmentValueChanged(AdjustmentEvent event) {
			actionsScroll.getVerticalScrollBar().setValue(mikroScroll.getVerticalScrollBar().getValue());
			lineNumberScroll.getVerticalScrollBar().setValue(mikroScroll.getVerticalScrollBar().getValue());
		}
	};
	
	private DocumentListener documentListener = new DocumentListener() {	
		@Override
		public void removeUpdate(DocumentEvent e) {
			notifyCodeCommentor();
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			notifyCodeCommentor();
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
			notifyCodeCommentor();
		}
	};
	
	private void notifyCodeCommentor() {
		synchronized (lock) {
			newCode = true;
			microCode = mikroCode.getText();
			lock.notifyAll();
		}
	}

	@Override
	public void run() {
		String c = null;
		while (true) {
			synchronized (lock) {
				while (!newCode) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
						// its ok
					}
				}
				
				c = microCode;
				newCode = false;
			}
			
			actions.setText(MikroParser.getComments(c));
		}	
	}
}
